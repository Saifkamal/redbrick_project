<?php

namespace App;

use App\SideBar;
use App\Page;
use App\Comment;
use App\Body;
use App\ServiceAttribute;

use Illuminate\Database\Eloquent\Model;

class Body extends Model
{
    protected $table='bodies';

    protected $fillable=['Name','Parent_id','Description','Body_type','Body_Icon','Upload_File'];


    public function pages(){

    	return $this->belongsTo(Page::class,'','id');
    }

    public function service_attributes(){

    	return $this->hasOne(ServiceAttribute::class,'service_id','id');
    }

}
