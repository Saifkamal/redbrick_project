<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Report;
use App\User;

class Comment extends Model
{
    protected $table='comments';

    protected $fillable=['user_id','report_id','title','description'];

    public function reports(){

    	return $this->belongsTo(Report::class,'','id');
    }

    public function users(){

    	return $this->belongsTo(User::class,'','id');
    }

}
