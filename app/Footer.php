<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Page;

class Footer extends Model
{
    protected $table='footers';

    protected $fillable=['Parent_id','F_header','F_content','F_header_type','F_content_type','File_Upload','Footer_Icon'];

    public function pages_footer(){

    	return $this->BelongsTo(Page::class,'','id');
    }

}
