<?php

namespace App\Http\Controllers;

use App\BaseAuth;
use Illuminate\Http\Request;
use Hash;
use App\User;

class BaseAuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('base_auth.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert_arr = [
            'name'              =>  $request->name,
            'email'             =>  $request->email,
            'password'          =>  Hash::make($request->password),
            'remember_token'    =>  csrf_token()
        ];
        $check = ($request->has('check') && $request->check) ? true:false;

        User::create($insert_arr,$check);

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BaseAuth  $baseAuth
     * @return \Illuminate\Http\Response
     */
    public function show(BaseAuth $baseAuth)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BaseAuth  $baseAuth
     * @return \Illuminate\Http\Response
     */
    public function edit(BaseAuth $baseAuth)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BaseAuth  $baseAuth
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BaseAuth $baseAuth)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BaseAuth  $baseAuth
     * @return \Illuminate\Http\Response
     */
    public function destroy(BaseAuth $baseAuth)
    {
        //
    }
}
