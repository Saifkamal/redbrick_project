<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Session;

class BaseAuthLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('base_auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('email',$request->login)
                    ->orWhere('name',$request->login)
                    ->orderBy('id','asc')
                    ->get(); 

        $col_name   = '';
        $col_value  = '';

        if ($user) {
            
            if (filter_var($request->login,FILTER_VALIDATE_EMAIL)) {
                $col_name   = 'email';
                $col_value  = (!empty($user[0]->email)) ? $user[0]->email : '';   
            }elseif(preg_match('/^[a-zA-Z ]*$/', $request->login)){
                $col_name   = 'name';
                $col_value  = (!empty($user[0]->name)) ? $user[0]->name : '' ;
            }else{
                $col_name    = 'email';
                $col_value   = (!empty($user[0]->email)) ? $user[0]->email : '' ;
            }

            $arr_login = [
                $col_name   =>  $col_value,
                'password'  =>  $request->password
            ];

            $remember = ($request->has('check') && $request->check) ? true : false;

            if (Auth::attempt($arr_login,$remember)) {
                $session_token  =  Session::put('msg','Congrats,Your Login is Successful!');
                return view('base_auth.logged_in'); 
            }else{
                return back();
            } 

        }else{
            return redirect('/index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
