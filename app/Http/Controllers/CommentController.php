<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Report;
use App\User;
use Illuminate\Support\Facades\Auth;
use Session;
use GuzzleHttp\Client;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request,Report $report){

        $token = $request->input('g-recaptcha-response');
        if ($token) {
            $client = new Client();
            $response = $client->post('https://www.google.com/recaptcha/api/siteverify',[
                'form_params'=> array(
                    'secret'    =>  '6LePgp8UAAAAAGiKu61CEfndBnkYYqwHHlxT6JXd',
                    'response'  =>  $token
                )
            ]);
            $result = json_decode($response->getBody()->getContents());
            if ($result->success) {
                if (Auth::user() && Session::has('msg')) {
                    $authenticated_user=User::where('id',$report->user_id)->first();
                    
                    $insert=['user_id'=>$authenticated_user->id,'report_id'=>$report->id,'title'=>$authenticated_user->name,'description'=>$request->mycomment];

                    Comment::create($insert);
                    return back();
                }else{
                    return redirect('/base_login_get');
                }        
            }else{
                return redirect('/index');
            }
        }else{

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $postscomment)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $postscomment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $postscomment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $postscomment)
    {
        Comment::where('id',$postscomment->id)->delete();

        return back();
    }
}
