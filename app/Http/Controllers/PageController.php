<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Nav;
use App\Body;
use App\SideBar;
use App\Footer;
use App\Comment;
use App\Report;
use App\ServiceAttribute;

use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $num=Page::create(['Page_no'=>$request->page_number]);
        $page=Page::find($num->id);

        return redirect()->route('new.page',$page);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        $nav=Nav::all();
        $n=Nav::where('Nav_Icon','!=',null)->get();
        $type=Body::pluck('Body_type');
        $footer11=Footer::where(['F_header_type'=>1,'F_Content_type'=>1])->get();
        $footer22=Footer::where(['F_header_type'=>2,'F_content_type'=>2])->get();
        $footer33=Footer::where(['F_Header_type'=>3,'F_content_type'=>3])->get();
        $footer44=Footer::where(['F_header_type'=>4,'F_content_type'=>4])->get();
        $report=Report::all();
        $comments=Comment::all();
        $service=ServiceAttribute::all();

        return view('Pages.index',compact('nav','n','page','type','footer11','footer22','footer33','footer44','comments','report','service'));
    }

    public function show_profile(Page $pages){

        $nav=Nav::all();
        $n=Nav::where('Nav_Icon','!=',null)->get();
        $body_info=Body::where(['Parent_id'=>1,'Body_type'=>3])->get();
        $footer11=Footer::where(['F_header_type'=>1,'F_Content_type'=>1])->get();
        $footer22=Footer::where(['F_header_type'=>2,'F_content_type'=>2])->get();
        $footer33=Footer::where(['F_Header_type'=>3,'F_content_type'=>3])->get();
        $footer44=Footer::where(['F_header_type'=>4,'F_content_type'=>4])->get();
        $report=Report::all();
        $comments=Comment::all();
        $service=ServiceAttribute::all();

        return view('Pages.index',compact('nav','n','pages','body_info','footer11','footer22','footer33','footer44','comments','report','service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
