<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Body;

class BodyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $body=Body::all();
        return view('base_UI.admin.body_dashboard',compact('body'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('base_UI.admin.body_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('body_file')) {
            $file_name=$request->file('body_file')->getClientOriginalName();
            $request->body_file->storeAs('public',$file_name);
            $insert=[
                'Name'          =>$request->name,
                'Parent_id'     =>$request->parent,
                'Description'   =>$request->body_description,
                'Body_type'     =>$request->body_type,
                'Body_Icon'     =>$request->body_icon,
                'Upload_File'   =>$file_name
            ];
            $remember=($request->has('remember') && $request->remember) ? true:false;
            Body::create($insert,$remember);

            return back();
        }else{

        }
        return redirect()->route('getbody',Auth::user()->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Body $body)
    {
        $body=Body::all();
        return view('UI.body_dashboard',compact('body'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Body $body)
    {
        return view('base_UI.admin.body_edit',compact('body'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Body $body)
    {
        if ($request->hasFile('body_file')) {
            $file_name=$request->file('body_file')->getClientOriginalName();
            $request->body_file->storeAs('public',$file_name);
            $update=[
                'Name'          =>$request->name,
                'Parent_id'     =>$request->parent,
                'Description'   =>$request->body_description,
                'Body_type'     =>$request->body_type,
                'Body_Icon'     =>$request->body_icon,
                'Upload_File'   =>$file_name
            ];
            $remember=($request->has('remember') && $request->remember) ? true:false;
            Body::where('id',$body->id)->update($update,$remember);

            return redirect()->route('body.index');
        }else{

        }
        return redirect()->route('body.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Body $body)
    {
        Body::where('id',$body->id)->delete();
        return back();
    }
}
