<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Footer;

class FooterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $footer=Footer::all();
        return view('base_UI.admin.footer_dashboard',compact('footer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('base_UI.admin.footer_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('f_upload')) {
            $file_name=$request->file('f_upload')->getClientOriginalName();
            $request->f_upload->storeAs('public',$file_name);
            $insert=[
                'Parent_id'         =>$request->f_parent,
                'F_header'          =>$request->f_header,
                'F_content'         =>$request->f_content,
                'F_header_type'     =>$request->f_header_type,
                'F_content_type'    =>$request->f_content_type,
                'File_Upload'       =>$file_name,
                'Footer_Icon'       =>$request->footer_icon
            ];
            $remember=($request->has('remember') && $request->remember) ? true: false;
            Footer::create($insert,$remember);

            return back();
        }else{

        }
        return redirect()->route('getfooter',Auth::user()->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Footer $footer)
    {
        $footer=Footer::all();
        return view('base_UI.admin.footer_dashboard',compact('footer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Footer $footer)
    {
        return view('base_UI.admin.footer_edit',compact('footer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Footer $footer)
    {
        if ($request->hasFile('f_upload')) {
            $file_name=$request->file('f_upload')->getClientOriginalName();
            $request->f_upload->storeAs('public',$file_name);
            $update=[
                'Parent_id'         =>$request->f_parent,
                'F_header'          =>$request->f_header,
                'F_content'         =>$request->f_content,
                'F_header_type'     =>$request->f_header_type,
                'F_content_type'    =>$request->f_content_type,
                'File_Upload'       =>$file_name,
                'Footer_Icon'       =>$request->footer_icon
            ];
            $remember=($request->has('remember') && $request->remember) ? true:false;
            Footer::where('id',$footer->id)->update($update,$remember);

            return redirect()->route('footer.index');
        }else{

        }
        return redirect()->route('footer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Footer $footer)
    {
        Footer::where('id',$footer->id)->delete();
        return back();
    }
}
