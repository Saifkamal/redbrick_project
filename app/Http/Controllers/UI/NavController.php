<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Nav;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UI\MakeUIRequest;


class NavController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nav=Nav::all();
        return view('base_UI.admin.nav_dashboard',compact('nav'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('base_UI.admin.nav_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert=[
            'Dropdown_link'         =>$request->drop_link,
            'Dropdown_link_type'    =>$request->drop_link_type,
            'Nav_Icon'              =>$request->nav_icon
        ];
        $remember=($request->has('remember') && $request->remember) ? true : false;
        Nav::create($insert,$remember);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Nav $nav)
    {
        $nav=Nav::all();
        return view('base_UI.admin.nav_dashboard',compact('nav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Nav $nav)
    {
        return view('base_UI.admin.nav_edit',compact('nav'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Nav $nav)
    {
        $update=[
            'Dropdown_link'         =>$request->drop_link,
            'Dropdown_link_type'    =>$request->drop_link_type,
            'Nav_Icon'              =>$request->nav_icon
        ];
        $remember=($request->has('remember') && $request->remember) ? true: false;
        Nav::where('id',$nav->id)->update($update,$remember);

        return redirect()->route('nav.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nav $nav)
    {
         Nav::where('id',$nav->id)->delete();
         return back();
    }
}
