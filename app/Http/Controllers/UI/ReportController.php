<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Report;
use App\Nav;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $report=Report::all();
        return view('base_UI.admin.report_up_dashboard',compact('report'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('base_UI.admin.up_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('up_file_report') && $request->hasFile('up_image')) {
            $file_name=$request->file('up_file_report')->getClientOriginalName();
            $request->up_file_report->storeAs('public/',$file_name);
            $file_image=$request->file('up_image')->getClientOriginalName();
            $request->up_image->storeAs('public',$file_image);
            $insert=[
                'user_id'               =>$request->user_id,
                'report_name'           =>$request->up_name,
                'writer'                =>$request->writer,
                'report_description'    =>$request->report_description,
                'upload_report'         =>$file_name,
                'upload_image'          =>$file_image
            ];
            $remember=($request->has('remember') && $request->remember) ? true:false;
            Report::create($insert,$remember);

            return redirect()->route('report.index');
        }else {
            return back()->withErrors(['upload_report'=>'Sorry,your file could not be uploaded!!!']);    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        $report=Report::all();
        return view('base_UI.admin.report_up_dashboard',compact('report'));
    }

    public function preview(){
        $report=Report::all();
        $nav=Nav::all();

        return view('UI.Body.preview_report',compact('report','nav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        return view('base_UI.admin.up_edit',compact('report'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Report $report)
    {    
        if ($request->hasFile('up_file_report') && $request->hasFile('up_image')) {       
            $file_name=$request->file('up_file_report')->getClientOriginalName();
            $request->up_file_report->storeAs('public',$file_name);
            $file_image=$request->file('up_image')->getClientOriginalName();
            $request->up_image->storeAs('public',$file_image);
            $credentials=[
                'user_id'               =>$request->user_id,
                'report_name'           =>$request->up_name,
                'writer'                =>$request->writer,
                'report_description'    =>$request->report_description,
                'upload_report'         =>$file_name,
                'upload_image'          =>$file_image
            ];
            $remember=($request->has('remember')&& $request->remember) ? true:false;
            Report::where('id',$report->id)->update($credentials,$remember);

            return redirect()->route('report.index');
        }else{
            return back()->withErrors(['upload_report'=>'unsuccesful attempt!!','upload_image'=>'unsuccessful attempt!!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        Report::where('id',$report->id)->delete();
        return back()->withErrors('Congratulations,your record has been deleted');
    }
}
