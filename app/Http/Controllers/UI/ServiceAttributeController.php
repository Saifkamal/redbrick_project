<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ServiceAttribute;

class ServiceAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service=ServiceAttribute::all();
        return view('base_UI.admin.service_attribute_dashboard',compact('service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('base_UI.admin.service_attribute_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $credentials=[
            'service_id'                =>$request->service_id,
            'project_category'          =>$request->project_category,
            'date'                      =>$request->date,
            'project_status'            =>$request->project_status,
            'project_ranking'           =>$request->project_ranking,
            'project_url'               =>$request->project_url
        ];
        $remember=($request->has('remember') && $request->remember) ? true:false;
        ServiceAttribute::create($credentials,$remember);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceAttribute $service)
    {
        $service=ServiceAttribute::all();
        return view('base_UI.admin.service_attribute_dashboard',compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceAttribute $service)
    {
        return view('base_UI.admin.service_attribute_edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,ServiceAttribute $service)
    {
        $credentials=[
            'service_id'                =>$request->service_id,
            'project_category'          =>$request->project_category,
            'date'                      =>$request->date,
            'project_status'            =>$request->project_status,
            'project_ranking'           =>$request->project_ranking,
            'project_url'               =>$request->project_url
        ];
        $remember=($request->has('remember') && $request->remember) ? true:false;
        ServiceAttribute::where('id','=',$service->id)->update($credentials,$remember);

        return redirect()->route('service.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceAttribute $service)
    {
        ServiceAttribute::where('id','=',$service->id)->delete();
        return back();
    }
}
