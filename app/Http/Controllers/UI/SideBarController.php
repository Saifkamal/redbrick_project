<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SideBar;

class SideBarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sidebar=SideBar::all();
        return view('base_UI.admin.side_dashboard',compact('sidebar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('base_UI.admin.side_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('side_file')) {            
            $file_name=$request->file('side_file')->getClientOriginalName();
            $request->file('side_file')->storeAs('public',$file_name);
            $insert=[
                'Parent_id'             =>$request->parent_id,
                'Name'                  =>$request->name,
                'Sidebar_type'          =>$request->sidebar_type,
                'File_Upload'           =>$file_name,
                'Description'           =>$request->description_side,
                'Sidebar_Icon'          =>$request->side_icon,
                'Linkedin'              =>$request->side_linkedin
            ];
            $remember=($request->has('remember') && $request->remember) ? true: false;
            SideBar::create($insert,$remember);

            return back();
        }else{

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SideBar $sidebar)
    {
        $sidebar=SideBar::all();
        return view('base_UI.admin.side_dashboard',compact('sidebar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SideBar $sidebar)
    {
        return view('base_UI.admin.side_edit',compact('sidebar'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SideBar $sidebar)
    {
        if ($request->hasFile('side_file')) {       
            $file_name=$request->file('side_file')->getClientOriginalName();
            $request->side_file->storeAs('public',$file_name);   
            $update=[
                'Parent_id'         =>$request->parent_id,
                'Name'              =>$request->name,
                'Sidebar_type'      =>$request->sidebar_type,
                'File_Upload'       =>$file_name,
                'Description'       =>$request->description_side,
                'Sidebar_Icon'      =>$request->side_icon,
                'Linkedin'          =>$request->side_linkedin
            ];
            $remember=($request->has('remember') && $request->remember) ? true:false;
            SideBar::where('id',$sidebar->id)->update($update,$remember);

            return redirect()->route('sidebar.index');         
        }else{

        }
        return redirect()->route('sidebar.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SideBar $sidebar)
    {
        Sidebar::where('id',$sidebar->id)->delete();
        return back();
    }
}
