<?php

namespace App\Http\Requests\UI;

use Illuminate\Foundation\Http\FormRequest;

class MakeUIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo'=> 'required|max:255|string',
            'Dropdown_link'=>'required|max:255|string',
            'Dropdown_link_type'=>'required|max:255|string',
            'Dropdown_link_description'=>'required|max:255|text'
        ];
    }
}
