<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nav extends Model
{
    protected $table='navs';

    protected $fillable=['Dropdown_link','Dropdown_link_type','Nav_Icon'];
}
