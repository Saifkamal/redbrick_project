<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Body;
use App\SideBar; 
use App\Footer;

class Page extends Model
{
    protected $table='pages';

    protected $fillable=['Page_no'];

    public function bodies(){

    	return $this->hasMany(Body::class,'Parent_id','id');
    }

    public function sidebars(){

    	return $this->hasMany(SideBar::class,'Parent_id','id');
    }

    public function footers(){

    	return $this->hasOne(Footer::class,'Parent_id','id');
    }
}
