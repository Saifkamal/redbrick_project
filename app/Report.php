<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Comment;

use App\User;

class Report extends Model
{
    protected $table='reports';

    protected $fillable=['user_id','report_name','report_description','upload_report','upload_image','writer'];

    public function comments(){

    	return $this->hasMany(Comment::class,'report_id','id');
    }

    public function users(){

    	return $this->belongsTo(User::class,'','id');
    }

}
