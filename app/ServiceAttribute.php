<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Body;

class ServiceAttribute extends Model
{
    protected $table='service_attributes';

    protected $fillable=['service_id','project_category','date','project_status','project_ranking','project_url'];

    public function bodies(){

    	return $this->belongsTo(Body::class,'','id');
    }
}
