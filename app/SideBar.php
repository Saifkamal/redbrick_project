<?php

namespace App;

use App\Body;

use App\Page;

use Illuminate\Database\Eloquent\Model;

class SideBar extends Model
{
    protected $table='side_bars';

    protected $fillable=['Parent_id','Name','Sidebar_type','File_Upload','Description','Sidebar_Icon','Linkedin'];

    public function pages_side(){

    	return $this->belongsTo(Page::class,'','id');
    }

}
