<?php

namespace App;

use App\Comment;
use App\Report;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'remember_token',
    ];

    protected $hidden = [
        'password',
    ];

    public function comments(){

        return $this->hasMany(Comment::class,'user_id','id');
    }

    public function reports(){

        return $this->hasMany(Report::class,'user_id','id');
    }
}
