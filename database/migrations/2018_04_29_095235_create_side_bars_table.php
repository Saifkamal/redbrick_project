<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSideBarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('side_bars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Parent_id')->nullable();
            $table->text('Name');
            $table->integer('Sidebar_type')->nullable();
            $table->string('File_Upload')->nullable();
            $table->string('Description')->nullable();
            $table->string('Sidebar_Icon')->nullable();
            $table->string('Linkedin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('side_bars');
    }
}
