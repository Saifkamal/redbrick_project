<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFootersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('footers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Parent_id')->nullable();
            $table->string('F_header');
            $table->string('F_content')->nullable();
            $table->integer('F_header_type')->nullable();
            $table->integer('F_content_type')->nullable();
            $table->string('File_Upload')->nullable();
            $table->string('Footer_Icon')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('footers');
    }
}
