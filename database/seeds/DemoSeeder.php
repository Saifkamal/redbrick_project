<?php

use Illuminate\Database\Seeder;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reports= factory('App\Report',5)->create();

        return $reports->each(function($report){

        	factory('App\Comment',10)->create(['report_id'=>$report->id]);
        });
    }
}
