@extends('UI.index')

	@php
		$i=$page->id;	
	@endphp

	@switch($i)
		@case(1)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-header')
				@include('UI.Body.body-content')
			@endsection

			@section('sideBar')
				@include('UI.SideBar.side-content')
				@include('UI.SideBar.side-footer')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(2)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-about-header')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(3)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.services-details')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(4)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.portfolio-details')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break
		
		@case(5)
			@section('navBar')
				@include('UI.Nav.nav')
				@include('UI.Nav.nav-contact')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(6)
			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-blog')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(9)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-profile')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(10)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-profile')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(11)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-profile')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(16)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-profile')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(17)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-profile')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(18)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-profile')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(19)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-profile')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(20)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-profile')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(21)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-profile')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(22)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-profile')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(23)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-profile')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@case(24)

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-profile')
			@endsection

			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
		@break

		@default

			@section('navBar')
				@include('UI.Nav.nav')
			@endsection

			@section('bodySection')
				@include('UI.Body.body-header')
				@include('UI.Body.body-content')
			@endsection

			@section('sideBar')
				@include('UI.SideBar.side-content')
				@include('UI.SideBar.side-footer')
			@endsection
			
			@section('footerSection')
				@include('UI.Footer.footer-header')
				@include('UI.Footer.footer-body')
			@endsection
	@endswitch

