@php 
    use App\Body;
    $bodies=$page->bodies();

    $var4=$bodies->where(['Parent_id'=>2,'Body_type'=>4])->get();
@endphp

@foreach($var4 as $item)
    <section class="main-slider-area">
        <ul class="main-slider slide">
            <li class="slide__item item-bg-1" style="background:url({{ asset('images/slider/computer_with_graph.jpg') }}) ;background-repeat: no-repeat;background-size: cover;height: 100vh;">
                <div class="slide-caption">
                    <h2 class="slide-caption__title">About <span>Red Brick</span></h2>
                    <p class="slide-caption__desc">{{ $item->Description }}</p>
                </div>
            </li>
            <li class="slide__item item-bg-2" style="background:url({{ asset('images/slider/9.jpg') }}) ;background-repeat: no-repeat;background-size: cover;height: 100vh;">
                <div class="slide-caption">
                    <h2 class="slide-caption__title">About <span>Red Brick</span></h2>
                    <p class="slide-caption__desc"> Its services range from developing questionnaire, data collection to reporting writing, market research to market activation and project planning to project evaluation.</p>
                    <a href="car-grid-3.html" class="btn">Learn More</a>
                </div>
            </li>
        </ul>
    </section> 
@endforeach