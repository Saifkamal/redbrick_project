   <!-- Page Title bar -->
    <section class="defult-page-title overlay-black" style="background:url({{ asset('images/slider/agro_bus_bd.jpg') }}) ;background-repeat: no-repeat;background-size: cover;height: 100vh;">
        <div class="container clearfix">
            <div class="row">
                <div class="container clearfix">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="page-title-box center991">
                                <div class="page-title-content">
                                  <h2>our <span>Blog</span></h2>
                                    <p>
                                      <a href="{{ route('new.page',$page->id) }}">Home</a> / <a href="{{ route('new.page',$page->id) }}">Blog</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
 
    <!-- Blog Start -->
    <section class="blog-sidebar-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        @foreach($report as $item)
                            <div class="col-lg-12 col-md-12 col-sm-12" id="accordion">
                                <div class="blog-col wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".0s">
                                      <div class="blog-img">
                                          <img src="{{ url('storage/'.$item->upload_image) }}" alt="">
                                          <div class="post-date">
                                              <h1>{{ $item->report_name }}</h1>
                                          </div>  
                                      </div> 
                                      <div class="blog-content">
                                            <p>Written By- <a href="https://www.facebook.com/mohammad.nakib" onclick="MyFunction()"><span>{{ $item->writer }}</span></a></p>
                                            <div class="info-bar clearfix" role="tab" id="headingOne">
                                                <ul>
                                                    <li><i class="fa fa-comments-o" aria-hidden="true"></i>Selected Comments</li>
                                                   
                                                    <li><i class="fa fa-pencil" aria-hidden="true"></i> <a href="{{ route('login') }}">Admin</a></li>
                                                   
                                                    <li><i class="fa fa-download" aria-hidden="true"></i> <a href="{{ route('report_down',$item->id) }}">Download Report</a></li>

                                                    <li><i class="fa fa-eye" aria-hidden="true"></i> <a href="{{ route('preview') }}">Preview</a></li>
                                                </ul>
                                            </div>
                                          <p>{{ str_limit($item->report_description,400) }}<a href="{{ route('preview') }}">See more</a></p>  
                                      </div>
                                      <div class="card">
                                          <div class="card-block">
                                              <form method="post" action="{{ route('comment.store',$item->id) }}">
                                                {{ csrf_field() }}
                                                  <div class="form-group">
                                                     <textarea class="form-control" name="mycomment">
                                                         
                                                     </textarea>
                                                  </div>
                                                  <div class="g-recaptcha" data-sitekey="6LePgp8UAAAAAGhU7P3q86ntyYpUAKE-Tc_dtcjr"></div>
                                                  <div class="form-group" style="margin-left: 10px;">
                                                      <button type="submit" class="btn btn-primary">Add Comment</button>
                                                  </div>
                                             </form> 
                                          </div>
                                      </div>  
                                      <div class="card" style="margin-left: 10px;">
                                          <div class="card-block">
                                              @if(Auth::check() || Auth::user() || Auth::guest())
                                                @foreach($item->comments as $comment)
                                                  <a href="">{{ $comment->title }}</a> : {{ $comment->description }} 
                                                  <p>{{ $comment->created_at->diffForHumans() }}</p>
                                                  @auth
                                                  <form method="post" action="{{ route('comment.delete',$comment->id) }}">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        {{ csrf_field() }}
                                                         <button type="submit"><i class="fa fa-trash"></i></button>
                                                  </form>
                                                  @endauth
                                                  <br>
                                                @endforeach
                                              @else
                                                @php 
                                                dd('third party!');
                                                @endphp
                                              @endif
                                          </div>
                                      </div>
                                  </div>
                            </div>
                        @endforeach
                        <div class="inner-pagination text-center">
                            <nav aria-label="Page navigation">
                                <ul class="pagination m0">
                                    <li>
                                        <a href="#" aria-label="Previous">
                                          <span aria-hidden="true">«</span>
                                        </a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">1</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">2</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">3</a>
                                    </li>
                                    <li>
                                        <a href="#" aria-label="Next">
                                          <span aria-hidden="true">»</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
          function myFunction(){
              window.open("https://www.facebook.com/mohammad.nakib");
          }
        </script>
    </section>

 