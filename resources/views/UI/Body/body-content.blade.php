@php 
    use App\Body;
    $bodies=$page->bodies();
    $var1=$bodies->where(['Body_type'=>1,'id'=>2])->get();
@endphp

@foreach($var1 as $item)
<!-- About Start -->
    <section class="about-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="about-col" id="About">
                        <h3>{{ $item->Name }}</h3>
                        <h5>Unique and creative</h5>
                        <p>{{ $item->Description }}</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="about-col-img clearfix">
                        <img src="{{ url('storage/'.$item->Upload_File) }}" alt="" class="wow fadeInRight" data-wow-duration="1s" data-wow-delay="0s"> 
                    </div>
                </div> 
            </div>
        </div>
    </section>
@endforeach
<!-- Service Start -->
                
<!-- Our Services four -->
<section class="services-style-four">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="section-title">
                    <div class="title-style clearfix">
                        <h2>Our <span>Services</span></h2>
                    </div>
                    <p>RedBrick has a nationwide experienced team which assures effective Data Collection, Market Activation and Project Implementation.</p>
                </div>
            </div>    
        </div>
        
        @php 
        $bodies=$page->bodies();
        $var2=$bodies->where(['Body_type'=>2,'Parent_id'=>1])->get();
        @endphp

        @foreach($var2 as $item)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 column-margin" id="accordion">
                <div class="services-features-four text-center">
                    <i class="fa fa-{{ $item->Body_Icon }}" aria-hidden="true"></i>
                    <div class="back-color">
                        <h4>{{ str_limit($item->Name,25) }}</h4>
                        <p id="{{ $item->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">{{ str_limit($item->Description,300) }}</p>
                        <a class="btn btn-default custom-btn-four my-btn-sm" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $item->id }}" aria-expanded="true" aria-controls="collapseOne">Read</a>
                    </div>
                </div>  
            </div>
        @endforeach    
    </div>  
</section>            
         
<!-- Portfolio Start -->
<section class="portfolio-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="section-title">
                    <div class="title-style clearfix">
                        <h2>Our Projects</h2>
                    </div>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="portfolio-col">
                    <!-- Filter Nav -->
                    <ul class="portfolio-nav">
                        <li class="*" data-filter="all"  tabindex="-1"> All </li>
                        <li data-filter="1"  tabindex="-1"> Selected </li>
                    </ul>
                    <!-- Filter Content -->
                    <div class="filtr-container">
                        @php 
                            $bodies=$page->bodies();    
                            $var3=$bodies->where(['Body_type'=>3,'Parent_id'=>1])->get();
                        @endphp
                        
                        @foreach($var3 as $item)
                            <div class="col-xs-6 col-sm-4 col-md-4 filtr-item" data-category="3, 2" data-sort="value">
                                <div class="box">
                                    <img src="{{ url('storage/'.$item->Upload_File) }}" alt="">
                                    <div class="box-content">
                                        <h3 class="title">{{ $item->Name }}</h3>
                                        <ul class="icon">
                                            <li>
                                                <a href="{{ url('storage/'.$item->Upload_File) }}" data-lightbox="lightbox" data-title="My caption"><i class="fa fa-search-plus" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('new.page_profile',$item->id) }}"><i class="fa fa-link"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 

<!-- Counter Start -->
<section class="counter-area parallax overlay-black" style="background: url({{ asset('images/bg/clients.jpg') }});background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6 fw-480">
                <div class="counter-col wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".0s">
                    <div class="counter">
                        <i class="icon icon-House"></i>
                        <span class="count">10000</span>
                        <span>+</span>
                        <p title="Number of Primary Data Collections">{{ str_limit('Number of Primary Data Collections',18) }}</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6 fw-480">
                <div class="counter-col wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".2s">
                    <div class="counter">
                        <i class="icon icon-Briefcase"></i>
                        <span class="count">350</span>
                        <span>+</span>
                        <p>Staffs</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6 fw-480">
                <div class="counter-col wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".4s">
                    <div class="counter">
                        <i class="icon icon-Users"></i>
                        <span class="count">10</span>
                        <span>+</span>
                        <p>Clients</p>
                    </div>
                </div>
            </div>


            <div class="col-md-3 col-sm-6 col-xs-6 fw-480">
                <div class="counter-col wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".6s">
                    <div class="counter">
                        <i class="icon icon-Cup"></i>
                        <span class="count">1</span>
                        <span></span>
                        <p>Awards</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



