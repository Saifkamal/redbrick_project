@php 
  use App\Body;
  $bodies=$page->bodies();
  $var=$bodies->where(['Body_type'=>1,'id'=>1])->get();
@endphp

@foreach($var as $item)
    <section class="main-slider-area">
        <ul class="main-slider slide">
            <li class="slide__item item-bg-1" style="background:url({{ asset('images/slider/computer_with_graph.jpg') }}) ;background-repeat: no-repeat;background-size: cover;height: 100vh;">
                <div class="slide-caption">
                    <h2 class="slide-caption__title">Welcome to <span>Red Brick</span></h2>

                    <p class="slide-caption__desc">{{ $item->Description }}</p>
                                    
                    <a href="{{ route('new.page',$item->id) }}" class="btn">Learn More</a>
                </div>
            </li>
            <li class="slide__item item-bg-2" style="background: url({{ asset('images/slider/night_view_bd.jpg') }});background-repeat: no-repeat;background-size: cover;height: 100vh;">
                <div class="slide-caption">
                    <h2 class="slide-caption__title">Sample and <span>Questionnaire Design</span></h2>
                    <p class="slide-caption__desc">Data Collection</p>
                    <a href="{{ route('new.page',$item->id) }}" class="btn">Learn More</a>
                </div>
            </li>
            <li class="slide__item item-bg-3" style="background: url({{ asset('images/slider/agro_bus_bd.jpg') }});background-repeat: no-repeat;background-size: cover;height: 100vh;">
                <div class="slide-caption">
                    <h2 class="slide-caption__title"><span></span> Data Analysis and  <span>creative</span> Report Writing</h2>
                    <p class="slide-caption__desc">Data Enhancement</p>
                    <a href="{{ route('new.page',$item->id) }}" class="btn">Learn More</a>
                </div>
            </li>
        </ul>
    </section>   
@endforeach
