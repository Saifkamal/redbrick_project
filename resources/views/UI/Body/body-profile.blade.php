@php
	$body_info=\App\Body::where(['id'=>$page->id,'Parent_id'=>1,'Body_type'=>3])->get();
@endphp

@foreach ($body_info as $element)
	<h3 class="text-center">{{ $element->Name }}</h3>
	<div>
		<img src="{{ url('storage/'.$element->Upload_File) }}">
		<p>
			{{ $element->Description }}
		</p>
	</div>
	<br>
@endforeach