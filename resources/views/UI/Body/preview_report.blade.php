
@extends('UI.index')

@section('navBar')
  	<header class="main-navbar">
		    <div class="topbar-area">
	          <div class="container">
            	  <div class="row">
  	                <div class="col-lg-6 col-md-6 col-sm-6 center767">
  	                    <ul class="topbar-info">
  	                        <li><p><i class="fa fa-envelope"></i><a href="#">muyedredbrick@gmail.com</a></p></li>
  	                        <li><p><i class="fa fa-phone"></i> +8801921434480</p></li>
  	                    </ul>
  	                </div>
  	                <div class="col-lg-6 col-md-6 col-sm-6 center767">
  	                    <div class="social-icon clearfix">
  	                        <ul>
    	                          @php 
    	                          $count=-1;
    	                          $array=['facebook','twitter','linkedin','google'];
    	                          @endphp
  	                            @foreach($nav as $n)
    		                            @php
    		                            $count++; 
    		                            @endphp
    		                            
                                    @if ($count>3)
    		                            @break
    		                            @endif
    		                            <li><a href="https://www.facebook.com/redbrickbd001/"><i class="fa fa-{{ str_contains($n->Nav_Icon,$array[$count])? $array[$count]:false}}" aria-hidden="true"></i></a></li>
  	                            @endforeach
  	                        </ul>
  	                    </div>
  	                </div>
              	</div>
	          </div>
        </div>
  	</header>
	  <div class="header-navbar" id="navbar-main">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-default">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index-one.html"><img src="{{ asset('images/logo/redbrick.png') }}" alt="" style="margin-top: 5px;">
                            </a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" data-hover="dropdown" data-animations="fadeIn">
                            <ul class="nav navbar-nav navbar-right">			
		                      	@foreach($nav as $page)
				                  	<li><a href="{{ route('new.page',$page->id) }}">{{ $page->Dropdown_link }}</a></li>
		                      	@endforeach	
                        			<li><a href="{{ route('login') }}">Admin</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>	
@endsection


@section('bodySection')
  	<section class="blog-sidebar-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        @foreach($report as $item)
                            <div class="col-lg-12 col-md-12 col-sm-12" id="accordion">
                                <div class="blog-col wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".0s">
                                    {{-- <div class="blog-img">
                                        <img src="{{ url('storage/'.$item->upload_image) }}" alt="">
                                        <div class="post-date">
                                            <h1>{{ $item->report_name }}</h1>
                                        </div>  
                                    </div> --}} 
                                    <div class="blog-content">
                                        <p>Written By- <a href="https://www.facebook.com/mohammad.nakib" onclick="MyFunction()"><span>{{ $item->writer }}</span></a></p>
                                        <div class="info-bar clearfix" role="tab" id="headingOne">
                                            {{-- <ul>
                                                <li><i class="fa fa-comments-o" aria-hidden="true"></i>Selected Comments</li>
                                               
                                                <li><i class="fa fa-pencil" aria-hidden="true"></i> <a href="{{ route('login') }}">Admin</a></li>
                                               
                                                <li><i class="fa fa-download" aria-hidden="true"></i> <a href="{{ route('report_down',$item->id) }}">Download Report</a></li>

                                                <li><i class="fa fa-eye" aria-hidden="true"></i> <a href="{{ route('preview') }}">Preview</a></li>
                                            </ul> --}}
                                        </div>
                                        <p>{{ $item->report_description }}</p>  
                                    </div>
                                    <div class="card">
                                        <div class="card-block">
                                            <form method="post" action="{{ route('comment.store',$item->id) }}">
                                              {{ csrf_field() }}
                                                <div class="form-group">
                                                   <textarea class="form-control" name="mycomment">
                                                       
                                                   </textarea>
                                                </div>
                                                <div class="form-group" style="margin-left: 10px;">
                                                    <button type="submit" class="btn btn-primary">Add Comment</button>
                                                </div>
                                           </form> 
                                        </div>
                                    </div>  
                                    <div class="card" style="margin-left: 10px;">
                                        <div class="card-block">
                                            @if(Auth::check() || Auth::user() || Auth::guest())
                                                @foreach($item->comments as $comment)
                                                    <a href="">{{ $comment->title }}</a> : {{ $comment->description }} 
                                                    <p>{{ $comment->created_at->diffForHumans() }}</p>
                                                    @auth
                                                    <form method="post" action="{{ route('postscomment.destroy',$comment->id) }}">
                                                          <input type="hidden" name="_method" value="DELETE">
                                                          {{ csrf_field() }}
                                                           <button type="submit"><i class="fa fa-trash"></i></button>
                                                    </form>
                                                    @endauth
                                                    <br>
                                                @endforeach
                                                @else
                                                @php 
                                                    dd('third party!');
                                                @endphp
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="inner-pagination text-center">
                            <nav aria-label="Page navigation">
                                <ul class="pagination m0">
                                    <li>
                                        <a href="#" aria-label="Previous">
                                          <span aria-hidden="true">«</span>
                                        </a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">»</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
          function myFunction(){
              window.open("https://www.facebook.com/mohammad.nakib");
          }
        </script>
    </section>
@endsection

@section('footerSection')
	
@endsection