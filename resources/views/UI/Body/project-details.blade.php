<!-- Page Title bar -->
<section class="defult-page-title overlay-black">
    <div class="container clearfix">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="page-title-box center991">
                    <div class="page-title-content">
                        <h2>Project <span>Details</span></h2>
                        <p><a href="index-one.html">Home</a> / <a href="#">Projects</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@php 
    use App\Body;
    $bodies=$page->bodies();
    $var7=$bodies->where('Parent_id',3)->get();
@endphp

@foreach($var7 as $item)
     <!-- Portfolio Details Start -->
    <section class="portfolio-details">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="details-col">
                        <img src="{{ url('storage/'.$item->Upload_File) }}" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="details-col">
                        <ul>
                            <li>
                                <i class="fa fa-hand-o-right" aria-hidden="true"></i> <strong>Project Category :</strong> Investment
                            </li>
                            <li>
                                <i class="fa fa-hand-o-right" aria-hidden="true"></i> <strong>Date :</strong> 21 March, 2018
                            </li>
                            <li>
                                <i class="fa fa-hand-o-right" aria-hidden="true"></i> <strong>Project Status :</strong> Running
                            </li>
                            <li>
                                <i class="fa fa-hand-o-right" aria-hidden="true"></i> <strong>Project Ranking :</strong> 
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-half-o" aria-hidden="true"></i>
                            </li>
                            <li>
                                <i class="fa fa-hand-o-right" aria-hidden="true"></i> <strong>Live Demo :</strong> <a href="#">www.YourSite.com</a>
                            </li>
                        </ul>
                        <p>{{ $item->Description }}</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="details-col">
                        <h3>{{ $item->Name }}</h3>
                        <p>{{ $item->Description }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endforeach