<!-- Page Title bar -->
<section class="defult-page-title overlay-black" style="background:url({{ asset('images/slider/agro_bus_bd.jpg') }}) ;background-repeat: no-repeat;background-size: cover;height: 100vh;">
    <div class="container clearfix">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="page-title-box center991">
                    <div class="page-title-content">
                        <h2>Services <span>Details</span></h2>
                        <p><a href="{{ route('welcome') }}">Home</a> / <a href="#">Services</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@php 
    $bodies=$page->bodies();
    $var5=$bodies->where('Parent_id',3)->get();
@endphp

@foreach($var5 as $item)
    <!-- Service Details Start -->
    <section class="portfolio-details">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="details-col">
                        <img src="{{ url('storage/'.$item->Upload_File) }}" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="details-col">
                        <ul>
                            @php 
                            $service=\App\Body::find($item->id);
                            @endphp

                            <li>
                                <i class="fa fa-hand-o-right" aria-hidden="true"></i> <strong>Project Category :</strong>{{ $service->service_attributes->project_category }} 
                            </li>
                            <li>
                                <i class="fa fa-hand-o-right" aria-hidden="true"></i> <strong>Date :</strong>{{ $service->service_attributes->date }} 
                            </li>
                            <li>
                                <i class="fa fa-hand-o-right" aria-hidden="true"></i> <strong>Project Status :</strong> {{ ($service->service_attributes->project_status==1) ? 'Completed' : 'Running' }}
                            </li>
                            <li>
                                <i class="fa fa-hand-o-right" aria-hidden="true"></i> <strong>Project Ranking :</strong>
                                
                                @php
                                for ($i=0; $i<$service->service_attributes->project_ranking ; $i++) {      
                                @endphp
                                <i class="fa fa-star" aria-hidden="true"></i>
                                @php 
                                }
                                @endphp
                            </li>
                            <li>
                                <i class="fa fa-hand-o-right" aria-hidden="true"></i> <strong>Live Demo :</strong> <a href="{{ $service->service_attributes->project_url }}">{{ $service->service_attributes->project_url }}</a>
                            </li>
                        </ul>
                        <p>{{ $item->Description }}</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="details-col">
                        <h3>{{ $item->Name }}</h3>
                        <p>{{ $item->Description }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endforeach