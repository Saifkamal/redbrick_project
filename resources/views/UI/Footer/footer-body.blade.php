<!-- Footer Start -->
<footer class="main-footer">
    <div class="container">
        <div class="row" id="Contact">
            @foreach($footer11 as $footer)
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-col about">
                        <h3>{{ $footer->F_header }}</h3>
                        <p>{{ $footer->F_content }}</p>
                        <ul>
                            <li>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                JU,Savar,Dhaka
                            </li>
                            <li>
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                muyedredbrick@gmail.com
                            </li>
                        </ul>
                    </div>
                </div>
            @endforeach
            @foreach($footer22 as $footer)
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-col twitter">
                        <h3>{{ $footer->F_header }}</h3>
                        <div class="twitter-box">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                            <p><a href="#">@Company Name</a> {{ $footer->F_content }} <br> <a href="#">{{ $footer->created_at->diffForHumans() }}</a></p>
                        </div>
                    </div>
                </div>
            @endforeach
            @foreach($footer33 as $footer)
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="footer-col links">
                        <h3>Our <span>pages</span></h3>
                        <ul>
                            <li><a href="{{ route('welcome') }}">Home</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
            @endforeach
            @foreach($footer44 as $footer)
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="footer-col newsletter">
                        <h3>{{ $footer->F_header }}</h3>
                        <p>{{ $footer->F_content }}</p>
                        <form class="input-group">
                            <div class="form-group">
                                <input class="form-control" type="email" placeholder="Enter Your Email">
                            </div>
                            <span class="input-group-btn">
                                <button class="btn btn-default sub-btn" type="submit">
                                SUBSCRIBE</button>
                            </span>
                        </form>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</footer>
<!-- Copyright Start -->
<section class="footer-copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <p>©2018. Designed by <a href="https://themeforest.net/user/set-theme" target="_blank">SET-Theme.</a> All Rights Reserved.</p>
            </div>
        </div>
    </div>
</section>