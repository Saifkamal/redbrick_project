<!-- Contact Start -->
    <section class="contact-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="contact-col contact-infobox">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <p>muyedredbrick@gmail.com</p>
                        <p>nakibredbrick@gmail.com</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="contact-col contact-infobox">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <p>+88019121434480</p>
                        <p>+88019121434480</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="contact-col contact-infobox">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <p>Office Address: JU Campus, Savar, Dhaka</p>
                    </div>
                </div>
            </div>
            <div class="row contact-form-row">
                <div class="col-md-7">
                    <div class="row">
                        <div class="contact-col">
                            <form id="ajax-contact" method="post" action="php/contact.php">
                                <div class="col-md-6">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Your Name" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Your Email"  required>
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="subject" class="form-control" placeholder="Subject" id="subject" required>
                                </div>
                                <div class="col-md-12">
                                    <div class="contact-textarea">
                                        <textarea class="form-control" rows="6" placeholder="Wright Message" id="message" name="message" required></textarea>
                                        <button class="btn btn-default my-btn text-center" type="submit" value="Submit Form">Send Message</button>
                                    </div>
                                </div>
                                <div id="form-messages"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="contact-col">
                        <img src="{{ asset('images/contact.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>