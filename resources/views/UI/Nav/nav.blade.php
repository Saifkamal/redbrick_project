<!-- Main Header Start -->
    <header class="main-navbar">
        <div class="topbar-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 center767">
                        <ul class="topbar-info">
                            <li><p><i class="fa fa-envelope"></i><a href="#">muyedredbrick@gmail.com</a></p></li>
                            <li><p><i class="fa fa-phone"></i> +8801921434480</p></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 center767">
                        <div class="social-icon clearfix">
                            <ul>
                                  @php 
                                  $count=-1;
                                  @endphp
                                  
                                  @foreach($n as $n)
                                      @php
                                      $count++; 
                                      $array=['facebook','twitter','linkedin','google'];
                                      @endphp
                                      
                                      <li>
                                          <a href="https://www.facebook.com/redbrickbd001/"><i class="fa fa-{{ str_contains($n->Nav_Icon,$array[$count])? $array[$count]:false}}" aria-hidden="true"></i>
                                          </a>
                                      </li>
                                  @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header navbar start -->
        <div class="header-navbar" id="navbar-main">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-default">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="index-one.html"><img src="{{ asset('images/logo/redbrick.png') }}" alt="" style="margin-top: 5px;">
                                </a>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" data-hover="dropdown" data-animations="fadeIn">
                                <ul class="nav navbar-nav navbar-right">            
                                    @foreach($nav as $page)
                                	      <li><a href="{{ route('new.page',$page->id) }}">{{ $page->Dropdown_link }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>



