<!-- Team Start -->
    <section class="team-area bg-f9">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="section-title">
                        <div class="title-style clearfix">
                            <h2>Experience Team</h2>
                        </div>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @php
                    use App\SideBar; 
                    $sidebars=$page->sidebars();
                    $var8=$sidebars->where('Sidebar_type',1)->get();
                @endphp

                @foreach($var8 as $sidebar)
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 fw-600">
                        <div class="team-single-col wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".0s">
                            <div class="team-img">
                                <a href="team-details.html"><img src="{{ url('storage/'.$sidebar->File_Upload) }}" alt="" ></a>
                                <div class="team-overlay">
                                    <div class="team-content">
                                        <h4><a href="team-details.html">{{ $sidebar->Name }}</a></h4>
                                        <p>{{ $sidebar->Description }}</p>
                                        <div class="team-social-group">
                                            <ul>
                                                <li>
                                                    <a href="{{ $sidebar->Sidebar_Icon }}"><i class="fa fa-facebook" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                                </li>
                                                <li>
                                                    <a href="{{ $sidebar->Linkedin }}"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

<!-- Blog Start -->
    <section class="blog-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="section-title">
                        <div class="title-style clearfix">
                            <h2>Blogs</h2>
                        </div>
                        <p>This is Our Blogs Section</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($report as $item)
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fw-600">
                        <div class="blog-col wow fadeIn" data-wow-duration="1s" data-wow-delay=".0s">
                            <div class="blog-img">
                                <img src="{{ url('storage/'.$item->upload_image) }}" alt="">
                                <div class="post-date">
                                    <h3>{{ $item->created_at->diffForHumans() }}</h3>
                                </div>
                            </div>
                            <div class="blog-content">
                                <h4><a href="blog-details.html">{{ str_limit($item->title,20) }}</a></h4>
                                <div class="info-bar clearfix">
                                    <ul>
                                        <li>
                                            <i class="fa fa-comments-o" aria-hidden="true"></i>{{ count($item->comments) }} Comments
                                        </li>
                                        <li>/</li>
                                         <li>
                                            <i class="fa fa-pencil" aria-hidden="true"></i> <a href="{{ route('login') }}">Admin</a>
                                        </li>
                                    </ul>
                                </div>
                                <p>{{ str_limit($item->description,300) }}</p>
                                <div class="card">
                                    <div class="card-block">
                                        <form action="{{ route('comment.store',$item->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <textarea name="mycomment" class="form-control">
                                                 
                                                </textarea>    
                                            </div>
                                            <div class="g-recaptcha" data-sitekey="6LePgp8UAAAAAGhU7P3q86ntyYpUAKE-Tc_dtcjr"></div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Add Comment</button>
                                            </div>
                                        </form> 
                                        @if(Auth::check() || Auth::user() || Auth::guest())
                                            @foreach($item->comments as $key)
                                                <a href="">{{ $key->title }}</a> : {{ $key->description }} 
                                                <p>{{ $key->created_at->diffForHumans() }}</p>
                                                @auth
                                                <form method="post" action="{{ route('comment.delete',$key->id) }}">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    {{ csrf_field() }}
                                                     <button type="submit"><i class="fa fa-trash"></i></button>
                                                </form>
                                                @endauth
                                                <br>
                                            @endforeach
                                        @else
                                            @php 
                                            dd('third party!');
                                            @endphp
                                        @endif
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>