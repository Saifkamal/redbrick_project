<!-- Testimonial Start -->
<section class="testimonial-area">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="section-title">
                    <div class="title-style clearfix">
                        <h2>Our clients Say</h2>
                    </div>
                    <p>The Content is Here!!</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="testimonial-col">
                    <div class="testimonial-carousel">
                        <div class="testimonial-item">
                            <div class="pic">
                                <img src="{{ asset('images/testimonial/1.jpg') }}" alt="">
                            </div>
                            <p class="description">
                                <q>The Content is Here</q>
                            </p>
                            <h3 class="title">Ashraf</h3>
                            <small class="post">- Web Designer</small>
                        </div>
                        <div class="testimonial-item">
                            <div class="pic">
                                <img src="{{ asset('images/testimonial/2.jpg') }}" alt="">
                            </div>
                            <p class="description">
                                <q>The Content is Here!!</q>
                            </p>
                            <h3 class="title">Saif</h3>
                            <small class="post">- Web Developer</small>
                        </div>
                        <div class="testimonial-item">
                            <div class="pic">
                                <img src="{{ asset('images/testimonial/3.jpg') }}" alt="">
                            </div>
                            <p class="description">
                                <q>The Content is Here!!</q>
                            </p>
                            <h3 class="title">Tahsin</h3>
                            <small class="post">- Grapic Designer</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>