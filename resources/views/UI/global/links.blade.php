    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">


    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">


    <!-- Responsive stylesheet  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">