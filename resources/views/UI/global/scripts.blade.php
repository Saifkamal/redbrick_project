    <!-- jQuery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <!-- Bootstrap Min js -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- bootstrap-dropdownhover js -->
    <script src="{{ asset('js/bootstrap-dropdownhover.min.js') }}"></script>
    <!-- imagesloaded JS -->
    <script src="{{ asset('js/imagesloaded.min.js') }}"></script>
    <!-- filterizr JS -->
    <script src="{{ asset('js/jquery.filterizr.min.js') }}"></script>
    <!-- typing js -->
    <script src="{{ asset('js/typing.js') }}"></script>
    <!-- VideoPopUp -->
    <script src="{{ asset('js/VideoPopUp.jquery.js') }}"></script>
    <!-- counterup -->
    <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
    <!-- waypoints -->
    <script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
    <!-- carousel -->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!-- jarallax -->
    <script src="{{ asset('js/jarallax.min.js') }}"></script>
    <!-- lightbox -->
    <script src="{{ asset('js/lightbox.min.js') }}"></script>
    <!-- youtubebackground JS -->
    <script src="{{ asset('js/jquery.youtubebackground.js') }}"></script>
    <!-- jquery scrollUp  JS -->
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <!-- wow.min JS -->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!-- Main Custom JS -->
    <script src="{{ asset('js/custom.js') }}"></script>