<!DOCTYPE html>
<html lang="{{ app()->getlocale() }}">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

	<title>RedBrick</title>

	<!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ asset('images/apple-favicon.png') }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}">
	@include('UI.global.links')
</head>
<body>

    <div class="preloader"></div>

    @yield('navBar')

    @yield('bodySection')

    @yield('sideBar')

    @yield('footerSection')

    @include('UI.global.scripts')

</body>
</html>