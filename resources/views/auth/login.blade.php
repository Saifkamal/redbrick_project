@extends('layouts.app')

@section('content')
<div class="card">
            <header class="card-header">
                <p class="card-header-title">
                  {{ __('Login') }}
                </p>
            </header>
            <div class="card-content">
                <div class="content">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                    <div class="field">
                      <label class="label">{{ __('E-Mail Address') }}</label>
                      <div class="control has-icons-left has-icons-right">
                        <input class="input is-danger {{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" id="email" name="email" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        <span class="icon is-small is-left">
                          <i class="fas fa-envelope"></i>
                        </span>
                        <span class="icon is-small is-right">
                          <i class="fas fa-exclamation-triangle"></i>
                        </span>
                      </div>
                    </div>

                    <div class="field">
                      <label class="label">{{ __('Password') }}</label>
                      <div class="control has-icons-left has-icons-right">
                        <input class="input is-success" id="password" type="password" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        <span class="icon is-small is-left">
                          <i class="fas fa-user"></i>
                        </span>
                        <span class="icon is-small is-right">
                          <i class="fas fa-check"></i>
                        </span>
                      </div>
                    </div>

                    <div class="field">
                        <label class="checkbox">
                          <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                          {{ __('Remember Me') }}
                        </label>
                    </div>

                    <div class="field is-grouped">
                      <div class="control">
                        <button class="button is-link">{{ __('Login') }}</button>
                        <a href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                      </div>
                    </div>  
                    </form>
                    
                </div>
            </div>
        </div>
@endsection
