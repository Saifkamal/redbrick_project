@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-content">
                    <form method="POST" action="{{ route('password.request') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="field">
                            <div class="control">
                                <label>{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="input is-primary {{ $errors->has('email') ? ' is-invalid' : '' }} " name="email" value="{{ $email ?? old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="field">
                            <div class="control">
                                <label>{{ __('Password') }}</label>
                                <input id="password" type="password" class="input is-primary {{ $errors->has('password') ? ' is-invalid' : '' }} " name="password" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="field">
                            <div class="control">
                                <label>{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="input is-primary {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }} " name="password_confirmation" required autofocus>
                            </div>
                        </div>

                        <div class="field">
                            <div class="control">
                                <button type="submit">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
