@extends('base_UI.admin.layout_admin')

@push('css-links')
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
@endpush

@section('bodyDash')
	<div class="card">
		<div class="card-header">
			<ul class="nav mt-2 mb-2 ml-2 mr-2">
				<li class="nav-item mr-2">
					<a href="{{ route('body.index') }}"><img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28"></a>
				</li>
				<li class="nav-item ml-2">
					<a href="{{ route('body.index') }}">Home</a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<a href="{{ route('body.create') }}"><button class="btn btn-success mt-2 mb-2">Create Body</button></a>
				</div>
				<div class="col-sm-2">
					<a href="{{ route('body.index') }}"><button class="btn btn-primary mt-2 mb-2">List of Bodies</button></a>
				</div>
				<div class="col-sm-6"></div>
			</div>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<table class="table table-bordered">
						<thead class="thead-dark">
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Body Type</th>
								<th>Parent ID</th>
								<th>Created_at</th>
								<th>Updated_at</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($body as $element)
								<tr>
									<td>{{ $element->id }}</td>
									<td>{{ $element->Name }}</td>
									<td>{{ $element->Body_type }}</td>
									<td>{{ $element->Parent_id }}</td>
									<td>{{ $element->created_at }}</td>
									<td>{{ $element->updated_at }}</td>
									<td>
										<a href="{{ route('body.edit',$element->id) }}"><i class="fas fa-pencil-alt"></i></a>
										<form action="{{ route('body.destroy',$element->id) }}" method="POST">
											@method('DELETE')
											{{ csrf_field() }}
											<button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
@endsection