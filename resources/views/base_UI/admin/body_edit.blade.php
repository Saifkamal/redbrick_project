@extends('base_UI.admin.layout_admin')

@section('bodyEdit')
	<div class="card">
		<div class="card-header">
			<ul class="nav">
				<li class="nav-item">
					<a href="{{ route('body.index') }}" class="nav-link"><img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick
						<br>_logo" width="112" height="28"></a>
				</li>
				<li class="nav-item"><a href="{{ route('body.index') }}">Home</a></li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<a href="{{ route('body.create') }}"><button class="btn btn-success">Create Body</button></a>
				</div>
				<div class="col-sm-2">
					<a href="{{ route('body.index') }}"><button class="btn btn-primary">List of Bodies</button></a>					
				</div>
				<div class="col-sm-6"></div>
			</div>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<form action="{{ route('body.update',$body->id) }}" method="POST" enctype="multipart/form-data">
						@method('PUT')
						{{ csrf_field() }}
						<h6>Name:</h6><input type="text" name="name" class="form-control" value="{{ $body->Name }}" placeholder="Enter Name" required>
						<br>
						<h6>Parent Id:</h6><input type="text" name="parent" class="form-control" value="{{ $body->Parent_id }}" placeholder="Enter Your Parent ID" required>
						<br>
						<h6>Description:</h6><textarea name="body_description" id="body_description" cols="30" rows="10" class="form-control" placeholder="Enter Body Description">{{ $body->Description }}</textarea>
						<br>
						<h6>Body_type:</h6><input type="text" name="body_type" value="{{ $body->Body_type }}" class="form-control" placeholder="Enter Body type">
						<br>
						<h6>Body_Icon URL:</h6><input type="text" name="body_icon" value="{{ $body->Body_Icon }}" class="form-control" placeholder="Enter Body Icon URL">
						<br>
						<h6>Upload File Here:</h6><input type="file" name="body_file" class="form-control">
						{{ url('storage/'.$body->Upload_File) }}
						<br>
						<br>
						<input type="checkbox" name="remember" checked="{{ !empty($remember) ? true:false }}"><h6>remember me</h6>
						<br>
						<input type="submit" name="submit" value="Update" class="btn btn-success mt-2 mb-2">
					</form>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
@endsection