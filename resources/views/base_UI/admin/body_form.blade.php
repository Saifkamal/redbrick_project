@extends('base_UI.admin.layout_admin')

@section('bodyForm')
	<div class="card">
		<div class="card-header">
			<ul class="nav ml-2 mt-2 mr-2 mb-2">
				<li class="nav-item mr-2">
					<a class="nav-link" href="{{ route('body.index') }}"><img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28"></a>
				</li>
				<li class="nav-item ml-2">
					<a href="{{ route('body.index') }}">Home</a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-2">
					
				</div>
				<div class="col-sm-2">
					
				</div>
				<div class="col-sm-8"></div>
			</div>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<form action="{{ route('body.store') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<h6>Name:</h6><input type="text" name="name" class="form-control" placeholder="Enter Your Name" required>
						<br>
						<h6>Parent Id:</h6><input type="text" name="parent" class="form-control" placeholder="Enter Your Parent Id" required>
						<br>
						<h6>Description:</h6><textarea name="body_description" id="body_description" cols="30" rows="10" class="form-control" placeholder="Enter Your Body Description"></textarea>
						<br>
						<h6>Body_type:</h6><input type="text" name="body_type" class="form-control" placeholder="Enter Body Type">
						<br>
						<h6>Body_Icon URL:</h6><input type="text" name="body_icon" placeholder="Enter Body Icon" class="form-control">
						<br>
						<h6>Upload File Here:</h6><input type="file" name="body_file" placeholder="Upload Your File" class="form-control">
						<br>
						<h6><input type="checkbox" name="remember">remember me</h6>
						<br>
						<input type="submit" value="Save" class="btn btn-success mt-2 mb-2">
					</form>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
@endsection