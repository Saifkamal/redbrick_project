@extends('base_UI.admin.layout_admin')
@push('css-links')
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
@endpush

@section('footerDash')
	<div class="card">
		<div class="card-header">
			<ul class="nav">
				<li class="nav-item">
					<a href="{{ route('footer.index') }}"><img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28" class="nav-link"></a>
				</li>
				<li class="nav-item">
					<a href="{{ route('footer.index') }}" class="nav-link">Home</a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row mt-2 mb-2 pb-2 pt-2">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<a href="{{ route('footer.create') }}"><button class="btn btn-success">Create Footer</button></a>
				</div>
				<div class="col-sm-2">
					<a href="{{ route('footer.index') }}"><button class="btn btn-primary">List of Footers</button></a>
				</div>
				<div class="col-sm-6"></div>
			</div>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<table class="table table-bordered">
						<thead class="thead-dark">
							<tr>
								<th>#</th>
								<th>Footer Header</th>
								<th>Footer Header Type</th>
								<th>Footer Content Type</th>
								<th>Created_at</th>
								<th>Updated_at</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($footer as $element)
								<tr>
									<td>{{ $element->id }}</td>
									<td>{{ $element->F_header }}</td>
									<td>{{ $element->F_header_type }}</td>
									<td>{{ $element->F_content_type }}</td>
									<td>{{ $element->created_at }}</td>
									<td>{{ $element->updated_at }}</td>
									<td>
										<a href="{{ route('footer.edit',$element->id) }}"><i class="fas fa-pencil-alt"></i></a>
										<form action="{{ route('footer.destroy',$element->id) }}">
											@method('PUT')
											{{ csrf_field() }}
											<button class="btn btn-danger" type="submit"><i class="fas fa-trash-alt"></i></button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="col-sm-2">
					
				</div>
			</div>
		</div>
	</div>
@endsection