@extends('base_UI.admin.layout_admin')

@section('footerForm')
	<div class="card">
		<div class="card-header">
			<ul class="nav">
				<li class="nav-item">
					<a href="{{ route('footer.index') }}"><img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28" class="nav-link"></a>
				</li>
				<li class="nav-item">
					<a href="{{ route('footer.index') }}" class="nav-link">Home</a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row mb-2 pb-2">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<a href="{{ route('footer.index') }}"><button class="btn btn-primary">List of Footers</button></a>
				</div>
				<div class="col-sm-8"></div>
			</div>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<form action="{{ route('footer.store') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<h6>Parent ID:</h6><input type="text" name="f_parent" class="form-control" placeholder="Enter Your Parent ID">
						<br>
						<h6>Footer Header:</h6><input type="text" name="f_header" class="form-control" placeholder="Enter Your Footer Header">
						<br>
						<h6>Footer Content:</h6><input type="text" name="f_content" class="form-control" placeholder="Enter Footer Content">
						<br>
						<h6>Footer Header Type:</h6><input type="text" name="f_header_type" class="form-control" placeholder="Enter Footer Header Type">
						<br>
						<h6>Footer Content Type:</h6><input type="text" name="f_content_type" class="form-control" placeholder="Enter Footer Content Type">
						<br>
						<h6>Upload File Here:</h6><input type="file" name="f_upload" class="form-control">
						<br>
						<h6>Footer Icon URL:</h6><input type="text" name="footer_icon" class="form-control">
						<br>
						<input type="checkbox" name="remember"><h6>Remember me</h6>
						<br>
						<input type="submit" name="submit" value="Save">
					</form>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
@endsection