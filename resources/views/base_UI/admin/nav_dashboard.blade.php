@extends('base_UI.admin.layout_admin')

@push('css-links')
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
@endpush

@section('navDash')
	<div class="card">
		<div class="card-header">
			<ul class="nav">
				<li class="nav-item">
					<a href="{{ route('nav.index') }}" class="nav-link">
						<img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28">
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ route('nav.index') }}" class="nav-link">Home</a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-2">
					
				</div>
				<div class="col-sm-8">
					<div class="row">
						<div class="col-sm-2">
							<a href="{{ route('nav.create') }}"><button class="btn btn-success mt-2 mb-2">Create NavBar</button></a>
						</div>
						<div class="col-sm-2">
							<a href="{{ route('nav.index') }}"><button class="btn btn-primary mt-2 mb-2">List of NavBar</button></a>
						</div>
						<div class="col-sm-8"></div>
					</div>
					<table class="table table-bordered">
						<thead class="thead-dark">
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Created_at</th>
								<th>Updated_at</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($nav as $element)
								<tr>
									<td>{{ $element->id }}</td>
									<td>{{ $element->Dropdown_link }}</td>
									<td>{{ $element->created_at }}</td>
									<td>{{ $element->updated_at }}</td>
									<td>
										<a href="{{ route('nav.edit',$element->id) }}"><i class="fas fa-pencil-alt"></i></a>
										<form action="{{ route('nav.destroy',$element->id) }}" method="POST">
											@method('DELETE')
											{{ csrf_field() }}
											<button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="col-sm-2">
					
				</div>
			</div>
		</div>
	</div>
@endsection