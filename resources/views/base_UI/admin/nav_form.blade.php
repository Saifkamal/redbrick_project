@extends('base_UI.admin.layout_admin')

@section('navForm')
	<div class="card">
		<div class="card-header">
			<ul class="nav">
				<li>
					<a href="{{ route('nav.index') }}"><img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28"></a>
				</li>
				<li class="nav-item">
					<a href="{{ route('nav.index') }}" class="nav-link">Home</a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<a href="{{ route('nav.create') }}"><button class="btn btn-success mt-2 mb-2">Create NavBar</button></a>
				</div>
				<div class="col-sm-2">
					<a href="{{ route('nav.index') }}"><button class="btn btn-primary mt-2 mb-2">List of NavBar</button></a>
				</div>
				<div class="col-sm-6"></div>
			</div>
			<div class="row mt-2 pt-2">
				<div class="col-sm-2">
					
				</div>
				<div class="col-sm-8">
					<form action="{{ route('nav.store') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<h6>Dropdown_link:</h6><input type="text" name="drop_link" class="form-control" placeholder="Enter Your Dropdown Link" required>
						<br>
						<h6>Dropdown_link_type:</h6><input type="text" class="form-control" name="drop_link_type" placeholder="Enter Your Drop Link type" required>
						<br>
						<h6>Nav Icon URL:</h6><input type="text" name="nav_icon" class="form-control" placeholder="Enter Your Nav Icon">
						<br>
						<input class="mr-2" type="checkbox" name="remember">remember me
						<br>
						<input type="submit" value="Submit" class="btn btn-success mt-2 mb-2">
					</form>	
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
@endsection