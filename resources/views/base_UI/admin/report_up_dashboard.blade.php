@extends('base_UI.admin.layout_admin')
@push('css-links')
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
@endpush

@section('reportDash')
	<div class="card">
		<div class="card-header">
			<ul class="nav">
				<li class="nav-item">
					<a href="{{ route('report.index') }}"><img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28" class="nav-link"></a>
				</li>
				<li class="nav-item">
					<a href="{{ route('report.index') }}" class="nav-link"></a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2 mb-2 pb-2">
					<a href="{{ route('report.create') }}"><button class="btn btn-success">Create Report Form</button></a>
				</div>
				<div class="col-sm-2">
					<a href="{{ route('report.index') }}"><button class="btn btn-primary">List of Report Form</button></a>
				</div>
				<div class="col-sm-6"></div>
			</div>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<table class="table table-bordered">
						<thead class="thead-dark">
							<tr>
								<th>User ID</th>
								<th>Report Title</th>
								<th>Report Description</th>
								<th>File Image</th>
								<th>Created_at</th>
								<th>Updated_at</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($report as $element)
								<tr>
									<td>{{ $element->id }}</td>
									<td>{{ $element->report_name }}</td>
									<td>{{ $element->report_description }}</td>
									<td>{{ $element->upload_image }}</td>
									<td>{{ $element->created_at }}</td>
									<td>{{ $element->updated_at }}</td>
									<td>
										<a href="{{ route('report.edit',$element->id) }}"><i class="fas fa-pencil-alt"></i></a>
										<form action="{{ route('report.destroy',$element->id) }}" method="POST">
											@method('DELETE')
											{{ csrf_field() }}
											<button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
@endsection