@extends('base_UI.admin.layout_admin')
@push('css-links')
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
@endpush

@section('serviceDash')
	<div class="card">
		<div class="card-header">
			<ul class="nav">
				<li class="nav-item">
					<a href="{{ route('service.index') }}" class="nav-link"><img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28"></a>
				</li>
				<li class="nav-item">
					<a href="{{ route('service.index') }}" class="nav-link">Home</a>
				</li>
			</ul>	
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2 mb-2 pb-2">
					<a href="{{ route('service.create') }}"><button class="btn btn-success">Create Service</button></a>
				</div>
				<div class="col-sm-2">
					<a href="{{ route('service.index') }}"><button class="btn btn-primary">List of Services</button></a>
				</div>
				<div class="col-sm-6"></div>
			</div>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<table class="table table-bordered">
						<thead class="thead-dark">
							<tr>
								<th>#</th>
								<th>Project_Category</th>
								<th>Date</th>
								<th>Project_Status</th>
								<th>Project_Ranking</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($service as $element)
								<tr>
									<td>{{ $element->id }}</td>
									<td>{{ $element->project_category }}</td>
									<td>{{ $element->date }}</td>
									<td>{{ $element->project_status }}</td>
									<td>{{ $element->project_ranking }}</td>
									<td>
										<a href="{{ route('service.edit',$element->id) }}"><i class="fas fa-pencil-alt"></i></a>
										<form action="{{ route('service.destroy',$element->id) }}" method="POST">
											@method('DELETE')
											{{ csrf_field() }}
											<button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
@endsection