@extends('base_UI.admin.layout_admin')

@section('serviceForm')
	<div class="card">
		<div class="card-header">
			<ul class="nav">
				<li class="nav-item">
					<a href="{{ route('service.index') }}" class="nav-link"><img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28"></a>
				</li>
				<li class="nav-item">
					<a href="{{ route('service.index') }}">Home</a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2 mb-2 pb-2">
					<a href="{{ route('service.index') }}"><button class="btn btn-primary">List of Services</button></a>
				</div>
				<div class="col-sm-8"></div>
			</div>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<form action="{{ route('service.store') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<h6>Service ID:</h6><input type="text" name="service_id" class="form-control" placeholder="Enter Your Service ID">
						<br>
						<h6>Project Category:</h6><input type="text" name="project_category" class="form-control" placeholder="Enter Project Category">
						<br>
						<h6>Date:</h6><input type="date" name="date" class="form-control" placeholder="Enter Date">
						<br>
						<h6>Project Status:</h6><input type="text" name="project_status" class="form-control" placeholder="Enter Project Status">
						<br>
						<h6>Project Ranking:</h6><input type="text" name="project_ranking" class="form-control" placeholder="Enter Project Ranking">
						<br>
						<h6>Project URL:</h6><input type="text" name="project_url" class="form-control" placeholder="Enter Project URL">
						<br>
						<input type="checkbox" name="remember"><h6>remember me</h6>
						<br>
						<input type="submit" name="submit" class="btn btn-success mt-2 mb-2" value="Save">
					</form>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
@endsection