@extends('base_UI.admin.layout_admin')

@section('sideEdit')
	<div class="card">
		<div class="card-header">
			<ul class="nav">
				<li class="nav-item">
					<a class="nav-link" href="{{ route('sidebar.index') }}"><img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28"></a>
				</li>
				<li class="nav-item">
					<a href="{{ route('sidebar.index') }}" class="nav-link">Home</a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<a href="{{ route('sidebar.index') }}"><button class="btn btn-primary">List of Sidebar</button></a>	
				</div>
				<div class="col-sm-8"></div>
			</div>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<form action="{{ route('sidebar.update',$sidebar->id) }}" method="POST" enctype="multipart/form-data">
						@method('PUT')
						{{ csrf_field() }}
						<h6>Parent_ID:</h6><input type="text" name="parent_id" class="form-control" placeholder="Enter Parent ID" value="{{ $sidebar->Parent_id }}">
						<br>
						<h6>Name:</h6><input type="text" name="name" class="form-control" placeholder="Enter Name" value="{{ $sidebar->Name }}" required>
						<br>
						<h6>SideBar_type:</h6><input type="text" name="sidebar_type" class="form-control" placeholder="Enter Sidebar_type" value="{{ $sidebar->Sidebar_type }}">
						<br>
						<h6>Upload File:</h6><input type="file" class="form-control" placeholder="Upload File" name="side_file">
						<br>
						<h6>Sidebar Description:</h6><textarea name="description_side" class="form-control" id="description_side" cols="30" rows="10">{{ $sidebar->Description }}</textarea>
						<br>
						<h6>Facebook URL:</h6><input type="text" name="side_icon" class="form-control" placeholder="Enter FB URL" value="{{ $sidebar->Sidebar_Icon }}">
						<br>
						<input type="checkbox" name="remember" checked="{{ !empty('remember') ? true:false }}"><h6>remember me</h6>
					</form>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
@endsection