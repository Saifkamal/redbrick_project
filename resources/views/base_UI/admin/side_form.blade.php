@extends('base_UI.admin.layout_admin')

@section('sideForm')
	<div class="card">
		<div class="card-header">
			<ul class="nav">
				<li class="nav-item">
					<a href="{{ route('sidebar.index') }}" class="nav-link"><img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28"></a>
				</li>
				<li class="nav-item">
					<a href="{{ route('sidebar.index') }}" class="nav-link">Home</a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row mb-2">
				<div class="col-sm-2"></div>
				<div class="col-sm-2 mb-2">
					<a href="{{ route('sidebar.index') }}"><button class="btn btn-success">List of Sidebar</button></a>
				</div>
				<div class="col-sm-8"></div>
			</div>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<form action="{{ route('sidebar.store') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<h6>Parent_ID:</h6><input type="text" name="parent_id" class="form-control" placeholder="Enter Parent Id">
						<br>
						<h6>Name:</h6><input type="text" name="name" class="form-control" placeholder="Enter Name" required>
						<br>
						<h6>SideBar_type:</h6><input type="text" name="sidebar_type" class="form-control" placeholder="Enter Sidebar Type" required>
						<br>
						<h6>Upload File:</h6><input type="file" name="side_file" class="form-control">
						<br>
						<h6>Sidebar Description:</h6><textarea name="description_side" id="description_side" cols="30" rows="10" class="form-control" placeholder="Enter Description" required></textarea>
						<br>
						<h6>Facebook URL:</h6><input type="text" name="side_icon" class="form-control" placeholder="Enter fb_URL">
						<br>
						<h6>LinkedIn URL:</h6><input type="text" name="side_linkedin" class="form-control" placeholder="Enter LinkedIn URL">
						<br>
						<input type="checkbox" name="remember"><h6>remember me</h6>
						<br>
						<input type="submit" name="submit" class="btn btn-success mt-2 mb-2">
					</form>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
@endsection