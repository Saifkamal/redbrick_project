@extends('base_UI.admin.layout_admin')

@section('reportEdit')
	<div class="card">
		<div class="card-header">
			<ul class="nav">
				<li class="nav-item">
					<a href="{{ route('report.index') }}" class="nav-link"><img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28" class="nav-link"></a>
				</li>
				<li class="nav-item">
					<a href="{{ route('report.index') }}" class="nav-link">Home</a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row mt-2 pt-2">
				<div class="col-sm-2"></div>
				<div class="col-sm-2 mb-2 pb-2">
					<a href="{{ route('report.index') }}"><button class="btn btn-primary">List of Reports</button></a>
				</div>
				<div class="col-sm-8"></div>
			</div>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<form action="{{ route('report.update',$report->id) }}" method="POST" enctype="multipart/form-data">
						@method('PUT')
						{{ csrf_field() }}
						<h6>User ID:</h6><input type="text" name="user_id" value="{{ $report->user_id }}" class="form-control" placeholder="Enter User ID">
						<br>
						<h6>Report Name:</h6><input type="text" name="up_name" value="{{ $report->report_name }}" class="form-control" placeholder="Enter Report Name">
						<br>
						<h6>Writer Name:</h6><input type="text" name="writer" value="{{ $report->writer }}" class="form-control" placeholder="Enter Writer Name">
						<br>
						<h6>Report Description:</h6><textarea name="report_description" id="report_description" class="form-control" cols="30" rows="10" placeholder="Enter Report Description">{{ $report->report_description }}</textarea>
						<br>
						<h6>Upload Report:</h6><input type="file" class="form-control" name="up_file_report">
						<br>
						{{ url('storage/'.$report->upload_report) }}
						<br>
						<h6>Upload Image:</h6><input type="file" class="form-control" name="up_image">
						<br>
						{{ url('storage/'.$report->upload_image) }}
						<br>
						<input type="checkbox" name="remember" checked="{{ !empty('remember') ? true:false }}"><h6>remember me</h6>
						<br>
						<input type="submit" name="submit" value="Save" class="btn btn-success mt-2 mb-2">
					</form>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
@endsection