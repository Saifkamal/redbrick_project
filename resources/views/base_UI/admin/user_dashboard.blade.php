@extends('base_UI.admin.layout_admin')



@section('userDash')
	<div class="card">
		<div class="card-header">
			<ul class="nav">
				<li class="nav-item">
					<a href="{{ route('user_dash') }}" class="nav-link">
						<img src="{{ asset('images/logo/redbrick.png') }}" alt="RedBrick_logo" width="112" height="28">
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ route('user_dash') }}" class="nav-link">Home</a>
				</li>
			</ul>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-2">
					
				</div>
				<div class="col-sm-8">
					<table class="table table-bordered table-hover">
						<thead class="thead-dark">
							<tr>
								<th>UserID</th>
								<th>UserName</th>
								<th>Email</th>
								<th>UserType</th>
								<th>Created_at</th>
								<th>Updated_at</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								@if (Auth::user())
									<td>{{ Auth::user()->id }}</td>
									<td>{{ Auth::user()->name }}</td>
									<td>{{ Auth::user()->email }}</td>
									<td>{{ Auth::user()->user_type }}</td>
									<td>{{ Auth::user()->created_at }}</td>
									<td>{{ Auth::user()->updated_at }}</td>
								@endif
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-sm-2">
					
				</div>
			</div>
		</div>
	</div>	
@endsection