<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Authentication Form for Registration</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="card">
		<div class="card-header">
			<h6>Authentication Form for Registration</h6>
		</div>
		<div class="card-body">
			<form action="{{ route('base_form_post') }}" method="POST">
				{{ csrf_field() }}
				<h6>UserName:</h6><input type="text" name="name" placeholder="Enter Your UserName" class="form-control">
				<br>
				<h6>Email:</h6><input type="email" name="email" class="form-control" placeholder="Enter Your Email">
				<br>
				<h6>Password:</h6><input type="password" name="password" class="form-control" placeholder="Enter Your Password">
				<br>
				<input type="checkbox" name="check" value="check">
				<br>
				<input type="submit" name="submit" value="Save" class="btn mt-2 mb-2">
			</form>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>