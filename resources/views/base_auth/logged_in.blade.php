<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>LoggedIn</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	@if (Session::has('msg'))		
		<div class="alert alert-success">
			<h6>{{ Session::get('msg') }}</h6>
		</div>
	@endif
	<table class="table table-bordered table-striped">
        <thead class="thead-dark">
            <tr>
              	<th>#</th>
              	<th>Name</th>
              	<th>Email</th>
                @if(Auth::user() && Auth::user()->user_type==1)
                    <th>Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
            <tr>   
                <td>{{ Auth::user()->id }}</td>
                <td>{{ Auth::user()->name }}</td>
                <td>{{ Auth::user()->email }}</td>
                <td>
                	@if (Auth::user()->user_type != 1)
	            		<a class="ml-2 mr-2" href="{{ route('user_dash',Auth::user()->id) }}">User List</a>
                	@else
	            		<a class="ml-2 mr-2" href="{{ route('user_dash',Auth::user()->id) }}">User List</a>
		        		<a class="ml-2 mr-2" href="{{ route('nav.index',Auth::user()->id) }}">Nav</a>
		                <a class="ml-2 mr-2" href="{{ route('body.index',Auth::user()->id) }}">Body</a>
		                <a class="ml-2 mr-2" href="{{ route('sidebar.index',Auth::user()->id) }}">SideBar</a>
		                <a class="ml-2 mr-2" href="{{ route('footer.index',Auth::user()->id) }}">Footer</a>
		                <a class="ml-2 mr-2" href="{{ route('report.index',Auth::user()->id) }}">Report Upload</a>
		                <a class="ml-2 mr-2" href="{{ route('service.index',Auth::user()->id) }}">Service Attributes</a>  
            		@endif
            	</td> 
            </tr>
        </tbody>
    </table>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>