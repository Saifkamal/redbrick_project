@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                  Accounts Dashboard
                </p>
                @if (Auth::user()->user_type==1)
                    <a href="{{ route('page_form') }}" class="button">Create Page</a>
                @endif
                <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fas fa-angle-down" aria-hidden="true"></i>
                  </span>
                </a>
            </header>
            <div class="card-content">
                <div class="content">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table">
                        <thead>
                            <tr>
                              <th><abbr title="">#</abbr></th>
                              <th>Name</th>
                              <th><abbr title="">Email</abbr></th>
                                @if(Auth::user() && Auth::user()->user_type==1)
                                    <th><abbr title="">Action</abbr></th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <tr>   
                                <td>{{ Auth::user()->id }}</td>
                                <td>{{ Auth::user()->name }}</td>
                                <td>{{ Auth::user()->email }}</td>
                                <td><a href="{{ route('getusers',Auth::user()->id) }}" title="">User List</a>
                                </td>
                                @if(Auth::user() && Auth::user()->user_type==1)
                                    <td><a href="{{ route('getnav',Auth::user()->id) }}" title="Nav">Nav</a>
                                    <td><a href="{{ route('getbody',Auth::user()->id) }}" title="Body">Body</a>
                                    <td><a href="{{ route('getside',Auth::user()->id) }}" title="SideBar">SideBar</a>
                                    <td><a href="{{ route('report.index',Auth::user()->id) }}" title="Report Upload">Report Upload</a></td>
                                    <td><a href="{{ route('getfooter',Auth::user()->id) }}" title="Footer">Footer</a>
                                    <td><a href="{{ route('service_attribute_dashboard',Auth::user()->id) }}" title="Service Attributes">Service Attributes</a></td>
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
