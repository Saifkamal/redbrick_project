<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
    <div id="app">
        <nav class="navbar is-transparent">
          <div class="navbar-brand">
            <a class="navbar-item" href="{{ url('/') }}">
              <img src="{{ asset('images/logo/redbrick.png') }}" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28">
            </a>

            <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>

          <div id="navbarExampleTransparentExample" class="navbar-menu">

            <div class="navbar-end">
              <div class="navbar-item">
                @guest
                <div class="field is-grouped">
                  <p class="control">
                    <a class="bd-tw-button button" href="{{ route('login') }}">
                      <span class="icon">
                        <i class="fas fa-sign-in-alt"></i>
                      </span>
                      <span>
                        {{ __('Login') }}
                      </span>
                    </a>
                  </p>
                  <p class="control">
                    <a class="button is-primary" href="{{ route('register') }}">
                      <span class="icon">
                        <i class="fas fa-registered"></i>
                      </span>
                      <span>{{ __('Register') }}</span>
                    </a>
                  </p>
                </div>
                @else
                    <div class="dropdown is-hoverable">
                        <div class="dropdown-trigger">
                            <button class="button" aria-haspopup="true" aria-controls="navbarSupportedContent">
                                <span>{{ Auth::user()->name }}</span>
                                <span class="icon is-small">
                                <i class="fas fa-angle-down" aria-hidden="true"></i>
                                </span>
                            </button>
                        </div>
                        <div class="dropdown-menu" id="navbarSupportedContent" role="menu">
                            <div class="dropdown-content">
                                <div class="dropdown-item">
                                    <form action="{{ route('logout') }}" method="post">
                                        {{ csrf_field() }}
                                        <input class="button" type="submit" value="Logout">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endguest
              </div>
            </div>
          </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
