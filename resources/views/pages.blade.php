@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Accounts Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    {{-- <form action="{{ route('pages.store') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                          <div class="field">
                            <label for="exampleInputEmail1">Page Number</label>
                            <input type="text" name="page_number" class="input" placeholder="Page Number #" required>
                          </div>
                          <div class="field">
                            <input type="checkbox" class="">Check me out
                            <label class="form-check-label" for="exampleCheck1">Check me out</label>
                          </div>
                          <button type="submit" class="btn btn-primary">Submit</button>
                    </form> --}}
                    <form action="{{ route('pages.store') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="field">
                            <label class="label">Page Number</label>
                            <div class="control">
                                <input class="input" name="page_number" type="text" placeholder="Page Number #" required>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control">
                                <label class="checkbox">
                                  <input type="checkbox" name="remember">
                                  I agree to the <a href="#">terms and conditions</a>
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="button">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
