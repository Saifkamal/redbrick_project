<?php
use App\Nav;
use App\Body;
use App\SideBar;
use App\Footer;
use App\Page;
use App\Comment;
use App\Report;
use App\ServiceAttribute;
use Illuminate\Support\Facades\Storage;
use App\Http\Response;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>''], function() {

	//Base Authentication
	Route::get('/index','BaseAuthController@index')->name('base_auth');
 	Route::post('/base_form_post','BaseAuthController@store')->name('base_form_post');
 	Route::get('/base_login_get','BaseAuthLoginController@index')->name('base_login_get');
 	Route::post('/base_login_post','BaseAuthLoginController@store')->name('base_login_post');

 	//Logout
 	Route::get('logout',function(){
 		Auth::logout();
        request()->session()->forget('msg');
 		return redirect('/');
 	});
 	//Web Layout
	Route::get('/', function () {
		
		$nav=Nav::all();
		$n=Nav::where('Nav_Icon','!=',null)->get();
		$page=Page::first();
		$type=Body::pluck('Body_type');
		$footer11=Footer::where(['F_header_type'=>1,'F_content_type'=>1])->get();
		$footer22=Footer::where(['F_header_type'=>2,'F_content_type'=>2])->get();
		$footer33=Footer::where(['F_header_type'=>3,'F_content_type'=>3])->get();
		$footer44=Footer::where(['F_header_type'=>4,'F_content_type'=>4])->get();
		$comments=Comment::all();
		$report=Report::all();
		$service=ServiceAttribute::all();

		return view('Pages.index',compact('page','type','nav','n','footer11','footer22','footer33','footer44','comments','report','service'));
	})->name('welcome');

	Route::get('test_login',function(){
		return view('welcome');
	})->name('test_login');
	
	Route::get('download_report/{report}',function(Report $report){
		$path=public_path('storage/'.$report->upload_report);
		return response()->download($path);
	})->name('report_down');

	Route::get('page',function(){
		return view('pages');
	})->name('page_form');

	Route::get('service_attribute_dashboard',function(){
		$service=ServiceAttribute::all();
		return view('UI/service_attribute_dashboard',compact('service'));
	})->name('service_attribute_dashboard');
});

//Home Group
Route::group(['prefix'=>''],function(){
	Auth::routes();
	Route::get('/home', 'HomeController@index')->name('home');
});

//Report+Blog Group
Route::group(['prefix'=>''],function(){
	Route::post('comment/{report}','CommentController@store')->name('comment.store');
	Route::delete('comment/{postscomment}','CommentController@destroy')->name('comment.delete');
	Route::get('new_page/{page}','PageController@show')->name('new.page');
	Route::get('new_page/{pages}','PageController@show_profile')->name('new.page_profile');
});

//Resource Controllers for CRUD
Route::group(['namespace'=>'UI'],function(){
	Route::resource('nav','NavController');
	Route::resource('body','BodyController');
	Route::resource('sidebar','SideBarController');
	Route::resource('footer','FooterController');
	Route::resource('report','ReportController');
	Route::resource('service','ServiceAttributeController');
	Route::get('link/{link}/content','NavController@link')->name('nav.link');
	Route::get('preview_report','ReportController@preview')->name('preview');
});

Route::group(['namespace' => '/'],function(){
	//UserList
	Route::get('user_list',function(){
		return view('base_UI.admin.user_dashboard');
	})->name('user_dash');
});
